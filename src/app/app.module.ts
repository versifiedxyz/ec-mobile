import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpModule } from '@angular/http';
import { SocialSharing } from '@ionic-native/social-sharing';
import { EmailComposer } from '@ionic-native/email-composer';
import { CallNumber } from '@ionic-native/call-number';
import { CacheModule } from 'ionic-cache-observable';
import { IonicStorageModule } from '@ionic/storage';
import { Network } from '@ionic-native/network';

import { WordpressService } from '../services/wordpress.service';
import { AuthenticationService } from '../services/authentication.service';



import { MyApp } from './app.component';
import { LandPage } from '../pages/land/land';
import { HomePage } from '../pages/home/home';
import { QuickInfo } from '../pages/info/info';
import { InfoPage } from '../pages/info/info';
import { FoodJoints } from '../pages/joints/joints';
import { JointPage } from '../pages/joint/joint';
import { PostPage } from '../pages/post/post';
import { News } from '../pages/news/news';
import { Direction } from '../pages/direction/direction';
import { SettingsPage } from '../pages/settings/settings';
import { LocationsPage } from '../pages/direction/direction';
import { AboutPage } from '../pages/settings/settings';
import { JointsProvider } from '../providers/joints/joints';
import { QuickInfoProvider,InstitutionProvider } from '../providers/info/info';

import { HttpClientModule } from '@angular/common/http';
import { OneSignal } from '@ionic-native/onesignal';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LandPage,
    QuickInfo,
    InfoPage,
    FoodJoints,
    JointPage,
    PostPage,
    LocationsPage,
    News,
    SettingsPage,
    AboutPage,
    Direction
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    CacheModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LandPage,
    QuickInfo,
    InfoPage,
    FoodJoints,
    JointPage,
    PostPage,
    LocationsPage,
    News,
    SettingsPage,
    AboutPage,
    Direction   
  ],
  providers: [
    StatusBar,
    SplashScreen,
    WordpressService,
    SocialSharing,
    EmailComposer,
    AuthenticationService,
    CallNumber,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpClientModule,
    JointsProvider,
    QuickInfoProvider,
    InstitutionProvider,
    OneSignal,
    Network
  ]
})
export class AppModule {}
