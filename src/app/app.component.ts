import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LandPage } from '../pages/land/land';
import { HomePage } from '../pages/home/home';

import { PostPage } from '../pages/post/post';

import { OneSignal } from '@ionic-native/onesignal';
import { ToastController } from 'ionic-angular';

import { isCordovaAvailable } from '../common/is-cordova-available';
import { oneSignalAppId, sender_id } from '../config';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private oneSignal: OneSignal,
    public toastCtrl: ToastController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // statusBar.styleDefault();
      splashScreen.hide();
      // initialize push notifications 
      this.initPush();
      this.checkInstitute()
    });
  }

  initPush() {
    if (isCordovaAvailable()) {
      this.oneSignal.startInit(oneSignalAppId, sender_id);
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
      // this.oneSignal.handleNotificationReceived().subscribe(data => this.onPushReceived(data.payload));
      this.oneSignal.handleNotificationOpened().subscribe(data => {
        // do something when a notification is opened
        // console.log('Tapped', data.notification.payload.additionalData.post);
        this.nav.push(PostPage, {
          postId: data.notification.payload.additionalData.post,
          reload: true,
        }).then(res => {
          console.log(res);
        }).catch(e => {
          console.log(e);
        });
      });
      this.oneSignal.endInit();
    }
  }
  checkInstitute() {
    this.rootPage = localStorage.getItem('institute') ? HomePage : LandPage;
  }
}

