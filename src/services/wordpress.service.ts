import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import * as Config from '../config';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';

@Injectable()
export class WordpressService {
  constructor(public http: Http){}

  getRecentPosts(categoryId:Array<String>, page:number = 1){
    //if we want to query posts by category
    let category_url = categoryId? ("&categories=" + categoryId): "";

    // console.log(Config.WORDPRESS_REST_API_URL
      // + 'posts?page=' + page
      // + category_url+'&_embed');
    
    return this.http.get(
      Config.WORDPRESS_REST_API_URL
      + 'posts?page=' + page
      + category_url+'&_embed')
    .map(res => res.json());
  }

  getAuthor(author){
    return this.http.get(Config.WORDPRESS_REST_API_URL + "users/" + author)
    .map(res => res.json());
  }

  getPostCategories(post){
    let observableBatch = [];

    post.categories.forEach(category => {
      observableBatch.push(this.getCategory(category));
    });

    return Observable.forkJoin(observableBatch);
  }

  //getting all recent posts including users institutions' post
  getRecentPostsByCategories(categories:Array<string>, pageNo:string){
		let url:string = Config.WORDPRESS_REST_API_URL + 'posts/?categories=';
		for(let catId of categories) {
			url += catId + ',';
		}
		if(url.charAt(url.length - 1) == ',') {
			url = url.substring(0, url.length - 1);
		}
		return this.http.get(url + '&page='+pageNo+'&_embed')
        .map(data => data.json());
	}
  getCategory(category){
    return this.http.get(Config.WORDPRESS_REST_API_URL + "categories/" + category)
    .map(res => res.json());
  }

  getCategories(){
		return this.http.get(Config.WORDPRESS_REST_API_URL + 'categories/?per_page=50&page=1')
        .map(res => res.json());
  }

  getPost(id){
		return this.http.get(Config.WORDPRESS_REST_API_URL + 'posts/'+id)
        .map(res => res.json());
	}
}
