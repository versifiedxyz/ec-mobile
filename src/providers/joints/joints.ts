import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// Remember to import RxJs dependencies.
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';

/*
  Generated class for the JointsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export interface JointModel {
  id: number;
  name: string;
}

@Injectable()
export class JointsProvider {

  constructor(private http: HttpClient) { }

  /**
   * Get some placeholder data.
   *
   * @returns {Observable<Placeholder>}
   */
  public all(): Observable<JointModel> {

    return Observable.defer(() => {
      let institute =  JSON.parse(localStorage.getItem('institute'));
      return this.http.get<JointModel>('https://versified.xyz/content/'+institute.key+'joints.json')
    });
  }

}