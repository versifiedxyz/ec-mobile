import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// Remember to import RxJs dependencies.
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';

/*
  Generated class for the quick info provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export interface InfoModel {
  id: number;
  name: string;
  loc: string;
  contact: string;
}

export interface InstitutionModel {
  id: number;
  name: string;
  desc: string;
}

@Injectable()
export class QuickInfoProvider {

  constructor(private http: HttpClient) { }

  /**
   * Get some quick info data.
   *
   * @returns {Observable<InfoModel>}
   */
  public all(): Observable<InfoModel> {
    return Observable.defer(() => {
      let institute =  JSON.parse(localStorage.getItem('institute'));
      
      return this.http.get<InfoModel>('https://versified.xyz/content/'+institute.key+'info.json')
    });
  }

}

@Injectable()
export class InstitutionProvider {

  constructor(private http: HttpClient) { }

  /**
   * Get some quick info data.
   *
   * @returns {Observable<InstitutionModel>}
   */
  public all(): Observable<InstitutionModel> {
    return Observable.defer(() => {
      return this.http.get<InstitutionModel>('https://versified.xyz/content/institutions.json')
    });
  }

}