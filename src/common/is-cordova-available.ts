export let isCordovaAvailable = () => {
	if (!(<any>window).cordova) {
		alert('This is a native feature. Please run from a device');
		return false;
	}
	return true;
};
