import { Component } from '@angular/core';
import { NavController, ModalController, NavParams, ViewController,LoadingController, Refresher } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

import { Observable } from 'rxjs/Observable';
import { QuickInfoProvider, InfoModel } from '../../providers/info/info';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Subscription } from 'rxjs/Subscription';

@Component({
  templateUrl: 'info.html'
})
export class QuickInfo {
  tips;
  filteredTips: Array<string> = [];
  public data$: Observable<InfoModel>;
  loading;
  /**
   * The cache instance for refreshing, etc.
   *
   * @type {Cache<QuickInfoProvider>}
   */
  public cache: Cache<InfoModel>;
  /**
   * Refresh subscription that can be cancelled when leaving the view.
   *
   * @type {Subscription}
   */
  public refreshSubscription: Subscription;
  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    cacheService: CacheService,
    private loadingCtrl: LoadingController,
    quickinfoprovider: QuickInfoProvider) {
    // The data we want to display on the page. 
    // This could be a user's profile or his list of todo items.
    const dataObservable = quickinfoprovider.all();
    this.loading = this.loadingCtrl.create();
    this.loading.present()

    // register a cache instance
    cacheService.register('quickinfo', dataObservable)
      .subscribe((cache) => {
        this.cache = cache;
        this.data$ = cache.get$;
        this.data$.subscribe(val => {
          this.tips = val;
          this.loading.dismiss();
          this.getTips();
        });
      });
      setTimeout(() => {
        if(this.loading) this.loading.dismiss();
    }, 6000);
  }

  ionViewWillLeave() {
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  ionViewWillLoad(){
    this.cache.refresh().subscribe();
  }  

  getTips(ev?: any) {
    const searchValue = ev ? ev.target.value : '';
    this.filteredTips = this.tips.filter((tip) => {
      return (tip.name.toLowerCase().indexOf(searchValue.toLowerCase()) > -1);
    });
  }

  getTip(tipId) {
    let modal = this.modalCtrl.create(InfoPage, tipId);
    modal.present();
  }

  /**
   * Refresh the cached data.
   *
   * @param {Refresher} refresher
   */
  public onRefresh(refresher?: Refresher): void {
    if (this.cache) {
      this.cache.refresh()
        .finally(() => this.refreshSubscription = null)
        .subscribe(() => refresher.complete(), (err) => {
          console.error('Something went wrong!', err);
          refresher.cancel();
          throw err;
        });
    } else {
      console.warn('Cache has not been initialized.');
      refresher.cancel();
    }
  }
}

@Component({
  templateUrl: 'tip.html'
})
export class InfoPage {
  tip;
  /**
   * The cache instance for refreshing, etc.
   *
   * @type {Cache<QuickInfoProvider>}
   */
  public cache: Cache<InfoModel>;

  constructor(public navParams: NavParams,
    public viewCtrl: ViewController,
    private callNumber: CallNumber,
    cacheService: CacheService,
    quickinfoprovider: QuickInfoProvider) {
    const dataObservable = quickinfoprovider.all();

    cacheService.register('quickinfo', dataObservable)
      .subscribe((cache) => {
        this.cache = cache;
        this.cache.get$.subscribe(val => {
          this.tip = val[navParams.get('tipId')];
        });
      });
  }

  call(num) {
    this.callNumber.callNumber(num, true).then(
      res => console.log('Launched dialer!', res)).catch(
      err => console.log('Error launching dialer', err)
    );
  }

  dismiss() {
    if(this.viewCtrl) this.viewCtrl.dismiss();
  }
}