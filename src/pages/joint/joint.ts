import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

import { JointsProvider, JointModel } from '../../providers/joints/joints';
import { Cache, CacheService } from 'ionic-cache-observable';

@Component({
  templateUrl: 'modal.html'
})
export class JointPage {
  joint; 

  /**
   * The cache instance for refreshing, etc.
   *
   * @type {Cache<JointsProvider>}
   */
  public cache: Cache<JointModel>;

  constructor(public navParams: NavParams,
    public viewCtrl: ViewController,
    private callNumber: CallNumber,
    cacheService: CacheService,
    jointsprovider: JointsProvider) {
    const dataObservable = jointsprovider.all();

    cacheService.register('food', dataObservable)
      .subscribe((cache) => {
        this.cache = cache;
        this.cache.get$.subscribe(val => {
          this.joint = val[navParams.get('jointId')];
          
        });
      });
  }

  call(num) {
    this.callNumber.callNumber(num, true).then(
      res => console.log('Launched dialer!', res)).catch(
      err => console.log('Error launching dialer', err)
      );
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}