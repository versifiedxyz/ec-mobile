import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { QuickInfo } from '../info/info';
import { FoodJoints } from '../joints/joints';
import { News } from '../news/news';
import { LocationsPage } from '../direction/direction';
import { SettingsPage } from '../settings/settings';

import { Subscription } from 'rxjs/Subscription';
import { Network } from '@ionic-native/network';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	connected: Subscription;
	disconnected: Subscription;
	institute = JSON.parse(localStorage.getItem('institute'))

	constructor(public navCtrl: NavController, 
		private network: Network, 
		private toast: ToastController) {
	}
	ionViewDidEnter() {
		this.connected = this.network.onConnect().subscribe(data => {
			this.displayNetworkUpdate('Woohoo! We have an active Internet connection!');
		}, error => console.error(error));

		this.disconnected = this.network.onDisconnect().subscribe(data => {
			this.displayNetworkUpdate('Ow! You can\'t enjoy the app with no Internet connection :(');
		}, error => console.error(error));
	}
	ionViewWillLeave() {
		this.connected.unsubscribe();
		this.disconnected.unsubscribe();
	}
	quickinfo() {
		this.navCtrl.push(QuickInfo);
	}
	openjoints() {
		this.navCtrl.push(FoodJoints);
	}
	morepage() {
		this.navCtrl.push(SettingsPage);
	}
	opennews() {
		this.navCtrl.push(News);
	}
	openlocations() {
		this.navCtrl.push(LocationsPage);
	}
	displayNetworkUpdate(connectionState: string) {
		this.toast.create({
			message: `${connectionState}`,
			duration: 3500
		}).present();
	}
}
