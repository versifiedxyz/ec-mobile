import { Component } from '@angular/core';
import { NavController, ToastController,NavParams,LoadingController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { LocationsPage } from '../direction/direction';
import { SettingsPage } from '../settings/settings';
import { OneSignal } from '@ionic-native/onesignal';

import { Subscription } from 'rxjs/Subscription';
import { Network } from '@ionic-native/network';

import { InstitutionProvider, InstitutionModel } from '../../providers/info/info';
import { Cache, CacheService } from 'ionic-cache-observable';


@Component({
	selector: 'page-land',
	templateUrl: 'land.html'
})
export class LandPage {
    connected: Subscription;
	disconnected: Subscription;

	institutions; 
	loading;

  /**
   * The cache instance for refreshing, etc.
   *
   * @type {Cache<InstitutionProvider>}
   */
  public cache: Cache<InstitutionModel>;

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		private network: Network, 
		public loadingCtrl: LoadingController,
		cacheService: CacheService,
		private oneSignal: OneSignal,
    	institutionprovider: InstitutionProvider,
		private toast: ToastController) {
		
		const dataObservable = institutionprovider.all();
		
		this.loading = this.loadingCtrl.create();
		this.loading.present();
		cacheService.register('institution', dataObservable)
			.subscribe((cache) => {
				this.cache = cache;
				this.cache.get$.subscribe(val => {
					this.loading.dismiss()
					this.institutions = val; //val[navParams.get('institutionId')];				
				});
			});	
			setTimeout(() => {
                this.loading.dismiss();
            }, 6000);				
	}

    ionViewDidEnter() {
        this.connected = this.network.onConnect().subscribe(data => {
            this.displayNetworkUpdate('Woohoo! We have an active Internet connection!');
        }, error => console.error(error));

        this.disconnected = this.network.onDisconnect().subscribe(data => {
            this.displayNetworkUpdate('Ow! You can\'t enjoy the app with no Internet connection :(');
        }, error => console.error(error));
	}
    ionViewWillLeave() {
        this.connected.unsubscribe();
        this.disconnected.unsubscribe();
    }
	morepage() {
		this.navCtrl.push(SettingsPage);
	}
	openhome(institute) {	
		localStorage.setItem('institute',JSON.stringify(institute));
		this.oneSignal.sendTag('institution', institute.key);
		this.navCtrl.setRoot(HomePage);
	}
	openlocations() {
		this.navCtrl.push(LocationsPage);
	}
    displayNetworkUpdate(connectionState: string) {
        this.toast.create({
            message: `${connectionState}`,
            duration: 3500
        }).present();
    }	
}
