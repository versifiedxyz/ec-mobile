import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { EmailComposer } from '@ionic-native/email-composer';
import { LandPage } from '../land/land';

// @IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  // Our local settings object
  options: any;

  settingsReady = false;
  aboutSettings = {
    page: 'about',
    pageTitleKey: 'SETTINGS_PAGE_ABOUT'
  };

  page: string = 'main';
  pageTitleKey: string = 'SETTINGS_TITLE';
  pageTitle: string;

  title: string= 'Try Easy Campus!';
  playLink: string= 'https://play.google.com/store/apps/details?id=xyz.versified.easycampus';
  description: string='Easy Campus is a simple app for becoming your best utility whilst you are on KNUST campus.';

  subSettings: any = SettingsPage;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private socialSharing: SocialSharing, 
    private emailComposer: EmailComposer,
    public modalCtrl: ModalController) {
  }

  shareapp() {
    this.socialSharing.shareWithOptions({
      message: `${this.title} 
      ${this.description}
      
      ${this.playLink}`
    }).then(() => {
      console.log('Shared!');
    }).catch((err) => {
      console.log('Oops, something went wrong:', err);
    });
  }  

  contact() {
    let email = {
        to: 'versifiedtechnology@gmail.com',
        subject: 'Easy Campus Feedback',
        body: '<br>Hi<br>',
        isHtml: true
    };
    this.emailComposer.open(email);
  }

  aboutpage() {
    let modal = this.modalCtrl.create(AboutPage);
    modal.present();
  }  


  ionViewDidLoad() {
  }
  landpage(){
    let modal = this.modalCtrl.create(LandPage);
    modal.present()
  }
  ionViewWillEnter() {
    this.page = this.navParams.get('page') || this.page;
    this.pageTitleKey = this.navParams.get('pageTitleKey') || this.pageTitleKey;
  }

  ngOnChanges() {
    console.log('Ng All Changes');
  }
}

@Component({
  templateUrl: 'about.html'
})
export class AboutPage {
  constructor(public viewCtrl: ViewController) {
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}