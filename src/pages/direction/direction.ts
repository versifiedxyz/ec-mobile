import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Platform, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;


@Component({
    templateUrl: 'locs.html'
})
export class LocationsPage {
  institute = JSON.parse(localStorage.getItem('institute'));
  locs;
  filteredLocs: Array<string> = [];

  constructor(public navCtrl: NavController) {
  }

  ionViewDidLoad() {
    this.locs = this.institute.locs
    this.getLocations();
  }

  getLocations(ev?: any) {
    const searchValue = ev ? ev.target.value : '';
    this.filteredLocs = this.locs.filter((pos) => {
      return (pos.nam.toLowerCase().indexOf(searchValue.toLowerCase()) > -1);
    });
  } 

  getDirection(loc) {
    this.navCtrl.push(Direction, {
        loc: loc.loc,
        nam: loc.nam
    });    
  }
}

@Component({
    selector: 'directions',
    templateUrl: 'direction.html'
})
export class Direction {
    @ViewChild('map') mapElement: ElementRef;
    @ViewChild('directionsPanel') directionsPanel: ElementRef;
    map: any;
    end;
    lat; lng;
    loc; nam: String;

    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer;
    constructor(public nav: NavController, public platform: Platform, public geolocation: Geolocation, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        this.loc = this.navParams.get('loc');
        this.nam = this.navParams.get('nam');

        this.loadMap();
    }

    loadMap() {
        this.geolocation.getCurrentPosition().then((position) => {
            this.lat = position.coords.latitude;
            this.lng = position.coords.longitude;

            let latLng = new google.maps.LatLng(this.lat, this.lng);
            let mapOptions = {
                center: latLng,
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true
            }
            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
            this.startNavigating();
        }, (err) => {
            console.log(err);
        });
    }

    startNavigating() {
        this.directionsDisplay.setMap(this.map);
        this.directionsDisplay.setPanel(this.directionsPanel.nativeElement);
        let latLng = new google.maps.LatLng(this.lat, this.lng);

        this.directionsService.route({
            origin: latLng,
            destination: this.loc,
            travelMode: google.maps.TravelMode['DRIVING']
        }, (res, status) => {
            if (status == google.maps.DirectionsStatus.OK) {
                this.directionsDisplay.setDirections(res);
            } else {
                console.warn(status);
            }
        });
    }
}