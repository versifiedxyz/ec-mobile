import { Component } from '@angular/core';
import { NavController, ModalController, Refresher, LoadingController } from 'ionic-angular';
import { JointPage } from '../joint/joint';
import { Observable } from 'rxjs/Observable';
import { JointsProvider, JointModel } from '../../providers/joints/joints';

import { Cache, CacheService } from 'ionic-cache-observable';
import { Subscription } from 'rxjs/Subscription';


@Component({
  templateUrl: 'joints.html'
})
export class FoodJoints {
  joints;
  filteredJoints: Array<string> = [];
  public data$: Observable<JointModel>;
  loading;
  /**
   * The cache instance for refreshing, etc.
   *
   * @type {Cache<JointsProvider>}
   */
  public cache: Cache<JointModel>;
  /**
   * Refresh subscription that can be cancelled when leaving the view.
   *
   * @type {Subscription}
   */
  public refreshSubscription: Subscription;

  constructor(public navCtrl: NavController,
    private loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    cacheService: CacheService,
    jointsprovider: JointsProvider) {

    // The data we want to display on the page. 
    // This could be a user's profile or his list of todo items.
    const dataObservable = jointsprovider.all();

    this.loading = this.loadingCtrl.create();
    this.loading.present()
    // register a cache instance
    cacheService.register('food', dataObservable)
      .subscribe((cache) => {
        this.cache = cache;
        this.data$ = cache.get$;
        this.data$.subscribe(val => {
          this.joints = val;
          this.loading.dismiss();
          this.getJoints();
        });
      });
  }

  ionViewWillLeave() {
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  ionViewWillLoad() {
    this.cache.refresh().subscribe();
  }

  getJoints(ev?: any) {
    const searchValue = ev ? ev.target.value : '';
    this.filteredJoints = this.joints.filter((joint) => {
      return (joint.name.toLowerCase().indexOf(searchValue.toLowerCase()) > -1);
    });
  }

  getJoint(jointId) {
    let modal = this.modalCtrl.create(JointPage, jointId);
    modal.present();
  }

  /**
   * Refresh the cached data.
   *
   * @param {Refresher} refresher
   */
  public onRefresh(refresher: Refresher): void {
    if (this.cache) {
      this.cache.refresh()
        .finally(() => this.refreshSubscription = null)
        .subscribe(() => refresher.complete(), (err) => {
          console.error('Something went wrong!', err);
          refresher.cancel();
          throw err;
        });
    } else {
      console.warn('Cache has not been initialized.');
      refresher.cancel();
    }
  }
}