import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { PostPage } from '../post/post';
import { WordpressService } from '../../services/wordpress.service';
import { AuthenticationService } from '../../services/authentication.service';



@Component({
    selector: 'page-news',
    templateUrl: 'news.html'
})
export class News {
    posts: Array<any> = new Array<any>();
    morePagesAvailable: boolean = true;
    loggedUser: boolean = false;

    categoryId: number;
    categoryTitle: string;

    cat: Array<string> = ['9'];//only general category id for now
    institute = JSON.parse(localStorage.getItem('institute'));

    loading;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public loadingCtrl: LoadingController,
        public wordpressService: WordpressService,
        public authenticationService: AuthenticationService) {
    }

    ionViewWillEnter() {
        this.fetchNews();
    }

    ionViewWillLeave() {
    }

    fetchNews() {
        this.morePagesAvailable = true;
        //if we are browsing a category
        this.categoryId = this.navParams.get('id');

        this.categoryTitle = this.navParams.get('title');
        if (!(this.posts.length > 0)) {
            this.loading = this.loadingCtrl.create();
            this.loading.present();
            //this.wordpressService.getRecentPosts(7)

            this.cat.push(this.institute.id);//push the institute id before fetching post
            this.wordpressService.getRecentPostsByCategories(this.cat, String(1))
                .subscribe(data => {
                    for (let post of data) {
                        post.excerpt.rendered = post.excerpt.rendered.split('<a')[0] + "</p>";
                        this.posts.push(post);
                    }
                    this.loading.dismiss();
                });
            // reduce this for slow connections? have to test 
            setTimeout(() => {
                this.loading.dismiss();
            }, 6000);
        }
    }
    doRefresh(refresher) {
        this.fetchNews();
        setTimeout(() => {
            console.log('Fetch news has ended');
            refresher.complete();
        }, 5000);
    }
    postTapped(event, post) {
        this.navCtrl.push(PostPage, {
            item: post
        });
    }
    doInfinite(infiniteScroll) {
        let page = (Math.ceil(this.posts.length / 10)) + 1;
        let loading = true;

        this.wordpressService.getRecentPosts(this.cat, page)
            .subscribe(data => {
                for (let post of data) {
                    if (!loading) {
                        infiniteScroll.complete();
                    }
                    post.excerpt.rendered = post.excerpt.rendered.split('<a')[0] + "</p>";
                    this.posts.push(post);
                    loading = false;
                }
            }, err => {
                this.morePagesAvailable = false;
            })
    }
}