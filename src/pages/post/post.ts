import { Component } from '@angular/core';
import { NavParams, NavController, LoadingController, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { WordpressService } from '../../services/wordpress.service';
import { Observable } from "rxjs/Observable";
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';

@Component({
  selector: 'page-post',
  templateUrl: 'post.html'
})
export class PostPage {

  post: any;
  user: string;
  trustedVideoUrl: SafeResourceUrl;
  categories: Array<any> = new Array<any>();
  morePagesAvailable: boolean = true;

  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private domSanitizer: DomSanitizer,
    public wordpressService: WordpressService) {
  }

  ionViewWillEnter() {
    this.morePagesAvailable = true;
    let loading = this.loadingCtrl.create();
    loading.present();

    if (this.navParams.get('reload')) {
      Observable.forkJoin(
        this.getPost())
        .subscribe(data => {
          this.post = data[0];
          if (this.post.acf.youtube_url.length > 0) {
            this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.post.acf.youtube_url);
          }
          Observable.forkJoin(
            this.getAuthorData(),
            this.getCategories())
            .subscribe(data => {
              this.user = data[0].name;
              this.categories = data[1];
              loading.dismiss();
            });
        });
    } else {
      this.post = this.navParams.get('item');
      if (this.post.acf.youtube_url.length > 0) {
        this.trustedVideoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.post.acf.youtube_url);
      }
      Observable.forkJoin(
        this.getAuthorData(),
        this.getCategories())
        .subscribe(data => {
          this.user = data[0].name;
          this.categories = data[1];
          loading.dismiss();
        });
    }
  }

  getAuthorData() {
    return this.wordpressService.getAuthor(this.post.author);
  }

  getCategories() {
    return this.wordpressService.getPostCategories(this.post);
  }

  getPost() {
    return this.wordpressService.getPost(this.navParams.get('postId'));
  }

  goToCategoryPosts(categoryId, categoryTitle) {
    this.navCtrl.push(HomePage, {
      id: categoryId,
      title: categoryTitle
    })
  }
}
